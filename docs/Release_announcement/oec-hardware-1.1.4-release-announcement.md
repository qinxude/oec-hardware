# 1.1.4-0 版本更新公告

## 问题修复

* [修复disk测试提示pci_num没有定义问题](https://gitee.com/openeuler/oec-hardware/pulls/166)
* [自动删除服务器ip行为，将其更改为由用户手动删除](https://gitee.com/openeuler/oec-hardware/pulls/168)
* [修改驱动的sha256获取方式](https://gitee.com/openeuler/oec-hardware/pulls/169)
* [修改网卡测试项里的日志记录信息](https://gitee.com/openeuler/oec-hardware/pulls/169)
* [修改错误的import](https://gitee.com/openeuler/oec-hardware/pulls/169)
* [解决光盘测试中出现的部分问题](https://gitee.com/openeuler/oec-hardware/pulls/170)
* [解决nvme用例测试中获取块大小失败问题](https://gitee.com/openeuler/oec-hardware/pulls/174)
* [memory日志格式修正，将error修改为info](https://gitee.com/openeuler/oec-hardware/pulls/175)
* [修复kabi测试用例在openEuler 22.03 LTS SP1系统上获取kernel.src值不正确的问题](https://gitee.com/openeuler/oec-hardware/pulls/176)
* [修复libswsds.so未被剥离的问题](https://gitee.com/openeuler/oec-hardware/pulls/177)


## 新增内容

* [基于服务器架构判断是否支持进行kabi测试](https://gitee.com/openeuler/oec-hardware/pulls/172)
* [在测试报告中新增几项板卡信息](https://gitee.com/openeuler/oec-hardware/pulls/178)
* [添加spdk测试用例](https://gitee.com/openeuler/oec-hardware/pulls/179)
* [添加spdk测试用例对应描述](https://gitee.com/openeuler/oec-hardware/pulls/187)
* [添加dpdk测试用例](https://gitee.com/openeuler/oec-hardware/pulls/180)
* [添加oec-hardware客户端依赖qperf、添加dpdk测试用例对应描述](https://gitee.com/openeuler/oec-hardware/pulls/191)
* [添加上游新增芯片信息及板卡信息](https://gitee.com/openeuler/oec-hardware/pulls/190)

## 预期更新内容

* 暂定
